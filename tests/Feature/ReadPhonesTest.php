<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ReadPhonesTest extends TestCase
{
    /** @test */
    public function a_user_can_view_all_phones()
    {
        $phones = factory('App\Phone', 50)->make();

        $this->get('/repository',['phones' => $phones])
        	->assertStatus(200);
    }

    /** @test */
    public function a_user_can_view_a_phone()
    {
        $phone = factory('App\Phone')->make([
                'id' => 1,
                'name' => 'nokiang'
            ]);

    	$this->get('/repository/1', ['phone' => $phone])
    		->assertStatus(200)
            ->assertSee('nokiang');
    }
}
