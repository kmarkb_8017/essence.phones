<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Phone::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->word,
        'make' => $faker->word,
        'model' => $faker->word,
        'name' => $faker->word,
        'type' => $faker->randomLetter,
        'tar_code' => $faker->word,
        'tar_name' => $faker->word,
        'tar_minues' => $faker->randomDigitNotNull,
        'tar_sms' => $faker->randomDigitNotNull,
        'tar_data' => $faker->randomDigitNotNull,
    ];
});

