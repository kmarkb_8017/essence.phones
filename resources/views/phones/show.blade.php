@extends('layouts.master')
@section('content')
    <section class="container">

        <hr>
        <div class="table">
            <h1>{{ $phone->name }}</h1>
            <hr>
            <p>Code: {{ $phone->code }}</p>
            <p>Make: {{ $phone->make }}</p>
            <p>Model: {{ $phone->model }}</p>
            <p>Type: {{ $phone->type }}</p>
            <p>Minutes: {{ $phone->tar_minues }}</p>
            <p>SMS: {{ $phone->tar_sms }}</p>
            <p>DATA: {{ $phone->tar_data }}</p>
            <p>TAR NAME: {{ $phone->tar_name }}</p>
            <p>TAR CODE: {{ $phone->tar_code }}</p>
        </div>
        <hr>
        <div>
            <a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
        </div>
    </section>
@endsection