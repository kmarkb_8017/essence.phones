@extends('layouts.master')
@section('content')
    <section class="container">
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>
                        Make
                        <span>
                            <a href="{{ url('repository/sort', ['make','asc']) }}"> 
                                <i class="fa fa-sort-asc" aria-hidden="true"></i> 
                            </a>
                            <a href="{{ url('repository/sort', ['make','desc']) }}">
                                <i class="fa fa-sort-desc" aria-hidden="true"></i> 
                            </a>
                        </span>
                    </th>
                    <th>
                        Model
                        <span>
                            <a href="{{ url('repository/sort', ['model','asc']) }}"> 
                                <i class="fa fa-sort-asc" aria-hidden="true"></i> 
                            </a>
                            <a href="{{ url('repository/sort', ['model','desc']) }}"> 
                                <i class="fa fa-sort-desc" aria-hidden="true"></i> 
                            </a>
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($phones as $phone)
                    <tr>
                        <td>
                            <a href="{{ url('repository', $phone->id) }}">{{ $phone->name }}</a>
                        </td>
                        <td>{{ $phone->make }}</td>
                        <td>{{ $phone->model }}</td>
                    </tr>
                @endforeach

            </tbody>
            
        </table>
        {{ $phones->links() }}

    </section>
@endsection