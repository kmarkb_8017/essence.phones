<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	return view('welcome');
});

// Repository Pattern
Route::get('repository', 'PhonesController@index');
Route::get('repository/{id}', 'PhonesController@show');
Route::get('repository/sort/{type}/{direction}', 'PhonesController@sortBy');
