<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\IPhonesRepository;

class PhonesController extends Controller
{

    // Define constructor class and inject Repository Interface
    public function __construct(IPhonesRepository $phone)
    {
        $this->phone = $phone;
    }

    /**
     * Display a listing of all the phones.
     *
     * @return \views\Repository\index.blade.php
     */
    public function index()
    {
        $phones = $this->phone->selectAll();

        return view('phones.index', compact('phones'));
    }

    /**
     * Display a specified phone detail.
     *
     * @param  int  $id
     * @return \views\Repository\show.blade.php
     */
    public function show($id)
    {
        $phone = $this->phone->selectOne($id);

        return view('phones.show', compact('phone'));
    }

    /**
     * Sort the phone results by make.
     *
     * @return \views\Respository\index.blade.php
     */
    public function sortBy($type, $direction)
    {
        $phones = $this->phone->sortBy($type, $direction);

        return view('phones.index', compact('phones'));
    }
}