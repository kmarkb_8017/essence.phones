<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    // Defines the table
    public $table = 'packages';

    public $timestamps = false;
}
