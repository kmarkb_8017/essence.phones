<?php

namespace App\Services;

use App\Phone;

class SortByService
{
	protected $phones;

	public function __construct(Phone $phone)
	{
		$this->phones = $phone;
	}

	public function sortBy($type, $direction)
	{
		if($type == 'model')
		{
			return $this->phones->orderBy('model', $direction)->paginate(20);
		}
		else if($type == 'make')
		{
			return $this->phones->orderBy('make', $direction)->paginate(20);
		}
	}
}