<?php

namespace App\Repositories;

use App\Phone;

use App\Services\SortByService;

class EloquentPhonesRepository implements IPhonesRepository
{
	protected $phones;
	protected $sortService;

	public function __construct(Phone $phones, SortByService $sortService)
	{
		$this->phones = $phones;
		$this->sortService = $sortService;
	}

	public function selectAll()
	{	
		return $this->phones->paginate(20);
	}

	public function selectOne($id)
	{
		return $this->phones->find($id);
	}

	public function sortBy($type, $direction)
	{
		return $this->sortService->sortBy($type, $direction);
	}
}