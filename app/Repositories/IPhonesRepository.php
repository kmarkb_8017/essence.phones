<?php

namespace App\Repositories;

interface IPhonesRepository
{
	public function selectAll();

	public function selectOne($id);

	public function sortBy($type, $direction);
}