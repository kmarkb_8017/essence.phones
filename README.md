# ESSENCE DIGITAL PHONES #

### The Coding Test ###
Assuming the attached data.csv file was imported into a MySQL database, either use bespoke code or the PHP framework of your choice to parse the data and present an aggregated and sortable list of phones by make and model. Clicking a phone should display details of the phone along with any tariffs available.

Your code should display good coding practices, adherence to standards and implementation of design patterns. Please use at least 3 design patterns. You may wish to include unit tests and code documentation.

### 3 design patterns ###

* Repository Pattern
* Service Layer Pattern
* Model View Controller

### Link to the live project ###

* http://essence.4ksoftware.io/
* By Karl Mark Balagtey

### How to setup project Essence Phones ###

* copy essence.phones to htdocs (XAMPP,MAMPP type local servers or homestead project folders)
* create .env file in root out of the .env.example.php file
* change DB_DATABASE, DB_USER, DB_PASSWORD to local setup
* create DB in localhost phpmyadmin, sequel pro or whatever your using to manage db's
* import essencedb.sql to DB
* navigate using cmd to project folder and run "composer install"
* to run Laravel built in server enter "php artisan run serve" then follow ip link to browser to open project
* otherwise use localhost/project-name for XAMPP/MAMPP or use the homestead way